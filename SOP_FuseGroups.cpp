/*
 * SOP_FuseGroups.h
 *
 *
 *    Copyright (c) 2010-2013 Sebastian H. Schmidt (sebastian<dot>h<dot>schmidt<at>googlemail<dot>com)
 *  Permission is hereby granted, free of charge, to any person obtaining a copy
 *  of this software and associated documentation files (the "Software"), to deal
 *  in the Software without restriction, including without limitation the rights
 *  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  copies of the Software, and to permit persons to whom the Software is
 *  furnished to do so, subject to the following conditions:
 *
 *  The above copyright notice and this permission notice shall be included in
 *  all copies or substantial portions of the Software.
 *
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 *  THE SOFTWARE.

 */



#include "SOP_FuseGroups.h"

#include <PRM/PRM_Default.h>
#include <PRM/PRM_Name.h>
#include <OP/OP_OperatorTable.h>
#include <OP/OP_AutoLockInputs.h>

#include <UT/UT_WorkArgs.h>
#include <GA/GA_ElementGroupTable.h>
#include <GU/GU_Detail.h>
#include <UT/UT_PtrArray.h>
#include <GA/GA_Group.h>

#define SHS_PARMDEF(name,prmName,prmLabel,numericDef,stringDef,help) \
    static PRM_Name     name##Name(prmName,prmLabel);\
    static PRM_Default  name##Default = PRM_Default(numericDef,stringDef);\
    static const char*  name##Help = help;


static PRM_Default	entityDefault(1);

static PRM_Name		groupMaskName("groupmask", "GroupMask");
static PRM_Default	groupMaskDefault = PRM_Default(0);
static const char*  groupMaskHelp = "the mask to specify which groups this needs to be ran on";




SHS_PARMDEF(distance,"distance","Distance",0.001,"","the distance to fuse points in")
SHS_PARMDEF(removeDeg,"deldegen","Remove Degenrated",1,"","removes degenerated polygons")

SHS_PARMDEF(keepUnused,"keepunusedpoints","Keep Unused Points",0,"","Keeps any points that were not part of a primitive before the fuse and, if Remove Degenerate Primitives is set, any points orphaned by removing degenerate polygons.")

SHS_PARMDEF(keepConsolidated,"keepconsolidatedpoints","Keep Consolidated Points",0,"","Keeps any points that are orphaned and not part of any polygons because they have been consolidated.")
SHS_PARMDEF(fast,"consolidatefast","Consolidate Fast",0,"","")
SHS_PARMDEF(accurate,"accurate","Use Accurate Distance",1,"","")




PRM_Template
SOP_FuseGroups::myTemplateList[] = {

    PRM_Template(PRM_INT_J		,1	,&PRMentityName			,&entityDefault  ,&PRMentityMenuPointsAndPrimitives),
    PRM_Template(PRM_STRING     ,1  ,&groupMaskName         ,&groupMaskDefault,0,0,0,0,0,groupMaskHelp),


    PRM_Template(PRM_FLT        ,1  ,&distanceName, &distanceDefault,0,0,0,0,0,distanceHelp),

    PRM_Template(PRM_TOGGLE     ,1  ,&removeDegName, &removeDegDefault,0,0,0,0,0,removeDegHelp),
    PRM_Template(PRM_TOGGLE     ,1  ,&keepUnusedName, &keepUnusedDefault,0,0,0,0,0,keepUnusedHelp),

    PRM_Template(PRM_TOGGLE     ,1  ,&keepConsolidatedName, &keepConsolidatedDefault,0,0,0,0,0,keepConsolidatedHelp),

    PRM_Template(PRM_TOGGLE     ,1  ,&fastName, &fastDefault,0,0,0,0,0,fastHelp),
    PRM_Template(PRM_TOGGLE     ,1  ,&accurateName, &accurateDefault,0,0,0,0,0,accurateHelp),


    PRM_Template(),
};


void
newSopOperator(OP_OperatorTable *table)
{
     table->addOperator(new OP_Operator("FuseGroups",
                                        "FuseGroups",
                                        SOP_FuseGroups::myConstructor,
                                        SOP_FuseGroups::myTemplateList,
                                         1,
                                         1,
                                         0));
}



OP_Node *
SOP_FuseGroups::myConstructor(OP_Network *net, const char *name, OP_Operator *op)
{
    return new SOP_FuseGroups(net, name, op);
}

const char *
SOP_FuseGroups::inputLabel(unsigned idx ) const
{
    if (idx==0)
    {
        return "Geometry to fuse points";
    }

}


SOP_FuseGroups::SOP_FuseGroups(OP_Network *net, const char *name, OP_Operator *op)   : SOP_Node(net, name, op)
{

}

SOP_FuseGroups::~SOP_FuseGroups()
{

}




OP_ERROR SOP_FuseGroups::cookMySop(OP_Context &context)
{
    OP_AutoLockInputs	autolock;

    if (autolock.lock(*this, context) >= UT_ERROR_ABORT) return error();

    UT_Interrupt*	 boss	 = UTgetInterrupt();

    this->duplicateSource(0,context);

    fpreal time 		= context.getTime();
    m_fast = getFast(time);
    m_accurate = getAccurate(time);
    m_distance = getDistance(time);


    int groupType = getGroupType(time);

    if (groupType == 1)
    {
        consolidatePointGroups(boss,time);
    }
    else
    {
        consolidatePrimGroups(boss,time);
    }




    if (!(boss->opInterrupt()) &&  (getRemoveDeg(time)))
    {
        gdp->cleanData(0,false,m_distance,!getKeepConsolidated(time),!getKeepUnused(time));
    }


    unlockInputs();
    return error();
}


GA_Size SOP_FuseGroups::consolidatePointGroups(UT_Interrupt*	 boss,fpreal time)
{
    UT_StringArray pointGroupNames;
    boss->opStart("Building Point Groups");
    SOP_FuseGroups::matchGroupList(gdp->pointGroups(),getGroupString(time), &pointGroupNames);
    boss->opEnd();
    GA_Size ret_val =0;

    if (pointGroupNames.entries()<1)
    {
        addWarning(SOP_ERR_BAD_POINTGROUP ,"no point-groups found matching the given pattern");
    }


    for (size_t i=0;i<pointGroupNames.entries();i++)
    {
        UT_String grpName = pointGroupNames(i);
        const GA_PointGroup* ptGroup = (GA_PointGroup*) gdp->pointGroups().find(grpName);

        if (m_fast)
        {
           ret_val+= gdp->fastConsolidatePoints(m_distance,ptGroup,false,m_accurate);
        }
        else
        {
            ret_val+= gdp->consolidatePoints(m_distance,ptGroup,false,false,m_accurate);
        }
        if (boss->opInterrupt())
            break;
    }
    return ret_val;
}

GA_Size SOP_FuseGroups::consolidatePrimGroups(UT_Interrupt*	 boss,fpreal time)
{
    UT_StringArray primGroupNames;
    boss->opStart("Building Primitive Groups");
    SOP_FuseGroups::matchGroupList(gdp->primitiveGroups(),getGroupString(time), &primGroupNames);
    boss->opEnd();

    GA_Size ret_val =0;
    if (primGroupNames.entries()<1)
    {
        addWarning(SOP_ERR_BAD_POINTGROUP ,"no primitive-groups found matching the given pattern");
    }

     bool forceConsAll = false;

    for (size_t i=0;i<primGroupNames.entries();i++)
    {
        UT_String grpName = primGroupNames(i);

        const GA_PrimitiveGroup* cPrimGrp = (GA_PrimitiveGroup*) gdp->primitiveGroups().find(grpName);

        GOP_Manager grpParser;

        const GA_ElementGroup *eGrp =  grpParser.convertGroupToType(*gdp,GA_ATTRIB_POINT,cPrimGrp);

        const GA_PointGroup* ptGroup = dynamic_cast<const GA_PointGroup*> ( eGrp );

        if (m_fast)
        {
            ret_val+=gdp->fastConsolidatePoints(m_distance,ptGroup,false,m_accurate);
        }
        else
        {
            ret_val+=gdp->consolidatePoints(m_distance,ptGroup,false,false,m_accurate);
        }
        if (boss->opInterrupt())
            break;

    }
    return ret_val;

}




void SOP_FuseGroups::matchGroupList(GA_ElementGroupTable &groupList, UT_String patternString, UT_StringArray *pGroupNames)
{

    UT_String    	groupName;
    UT_WorkArgs 	patternArgs;
    patternString.tokenize(patternArgs," ");
    pGroupNames->clear();

    UT_PtrArray<  const GA_ElementGroup  * > grplist;
    groupList.getList(grplist);

    for (unsigned int i=0;i<grplist.entries();i++)
    {
        groupName.harden(grplist(i)->getName());
        if (groupName.matchPattern(patternArgs))
        {
            pGroupNames->append(groupName);
        }
    }
}


int SOP_FuseGroups::getGroupType(float time)
{
    return evalInt(PRMentityName.getToken(),0,time);
}


UT_String SOP_FuseGroups::getGroupString(float time)
{
    UT_String ret_val;
    evalString(ret_val,groupMaskName.getToken(),0,time);
    return ret_val;
}

bool SOP_FuseGroups::getFast(float time)
{
    return evalInt(fastName.getToken(),0,time) == 1;
}

bool SOP_FuseGroups::getAccurate(float time)
{
    return evalInt(accurateName.getToken(),0,time) == 1;
}

float SOP_FuseGroups::getDistance(float time)
{
    return evalFloat(distanceName.getToken(),0,time);
}
bool SOP_FuseGroups::getRemoveDeg(float time)
{
    return evalInt(removeDegName.getToken(),0,time) == 1;
}

bool SOP_FuseGroups::getKeepConsolidated(float time)
{
    return evalInt(keepConsolidatedName.getToken(),0,time) == 1;
}

bool SOP_FuseGroups::getKeepUnused(float time)
{
    return evalInt(keepUnusedName.getToken(),0,time) == 1;
}
