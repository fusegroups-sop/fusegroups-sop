#ifndef SOP_FUSEGROUPS_H
#define SOP_FUSEGROUPS_H


#include <UT/UT_DSOVersion.h>


/*
 * SOP_FuseGroups.h
 *
 *
 *    Copyright (c) 2013 Sebastian H. Schmidt (sebastian<dot>h<dot>schmidt<at>googlemail<dot>com)
 *  Permission is hereby granted, free of charge, to any person obtaining a copy
 *  of this software and associated documentation files (the "Software"), to deal
 *  in the Software without restriction, including without limitation the rights
 *  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  copies of the Software, and to permit persons to whom the Software is
 *  furnished to do so, subject to the following conditions:
 *
 *  The above copyright notice and this permission notice shall be included in
 *  all copies or substantial portions of the Software.
 *
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 *  THE SOFTWARE.

 */


#include <GU/GU_Detail.h>

#include <SOP/SOP_Node.h>

class SOP_FuseGroups: public SOP_Node
{
public:
    static OP_Node  *myConstructor(OP_Network*,
                 const char *,
                 OP_Operator *);
    static PRM_Template myTemplateList[];

 protected:
                        SOP_FuseGroups(OP_Network *net, const char *name, OP_Operator *op);
    virtual 			~SOP_FuseGroups();

    virtual OP_ERROR  	cookMySop(OP_Context &context);

    //virtual unsigned	disableParms();

    //! @note: returns the label for the input
    //! @parm idx: input index
    virtual const char  *inputLabel(unsigned idx) const;

 public:


 private:
    //! @brief: consolidates points based on the point groups
    GA_Size consolidatePointGroups(UT_Interrupt*	 boss,fpreal time);

    //! @brief: consolidates points based on the primitive  groups
    GA_Size consolidatePrimGroups(UT_Interrupt*	 boss,fpreal time);

 private:
    //! @returns: returns a list of group-names based on a pattern and a list of existing groups
    void matchGroupList(GA_ElementGroupTable &groupList, UT_String patternString, UT_StringArray *pGroupNames);

    //! @returns: if either point or primitive groups are used for fusing the points
    int getGroupType(float time);

    //! @returns: the group-mask string
    UT_String getGroupString(float time);

    //! @returns: if fast or non-fast consolidation should be used for fusing
    bool  getFast(float time);

    //! @returns: if the accurate-distancing should be used
    bool  getAccurate(float time);

    //! @returns: the distance within points should be fused
    float getDistance(float time);

    //! @returns: if the user chose that  degenerated primitives should be removed
    bool  getRemoveDeg(float time);

    //! @returns: if the user chose to keep  consolidated points
    bool getKeepConsolidated(float time);

    //! @returns if unsed points should be kept
    bool getKeepUnused(float time);

  private:

    float m_distance;
    bool m_fast,m_accurate,m_forceConsAll;

};

#endif // SOP_FUSEGROUPS_H
